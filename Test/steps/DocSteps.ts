import { defineSupportCode } from 'cucumber'
import {DocPage} from "../pages/DocPage";

defineSupportCode(({Given, When, Then, setDefaultTimeout }) => {
    setDefaultTimeout(20000);
    
    var docpage = new DocPage();

    Then(/^I verify docs page$/, async () => {
        await docpage.getAngularText();
    });

});
