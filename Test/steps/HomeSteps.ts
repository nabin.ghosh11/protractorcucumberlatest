import { defineSupportCode } from 'cucumber'
import { HomePage } from "../pages/HomePage";
import { expect } from 'chai'
//import { CourseDetailsPage } from "../pages/CourseDetails";
import { browser } from 'protractor';

defineSupportCode(({Given, When, Then,setDefaultTimeout }) => {
    
    var homePage = new HomePage();
    setDefaultTimeout(20000);
    Given(/^I navigate to application$/, async () => {
       //
        await homePage.OpenBrowser("https://angular.io/");
    });

     When(/^I click on features$/, async()=> {
       await homePage.clickFeatures();
    });

    When(/^I click on docs$/, async()=> {
        await homePage.clickDoc();
    });
});