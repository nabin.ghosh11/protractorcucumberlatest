//import the class
import { browser, element, by, protractor, $$, $ } from 'protractor';
import { IdentificationType, BasePage } from "./BasePage";


const Locators = {


    getStartedButton: {
        type: IdentificationType[IdentificationType.Css],
        value: "a.button"
    },

    featuresLink :{
        type: IdentificationType[IdentificationType.Css],
        value: "a[title='Features']"
    },

    docLink :{
        type: IdentificationType[IdentificationType.Css],
        value: "a[title='Docs'] span"
    }
}


export class HomePage extends BasePage {

    getStartedButton =this.ElementLocator(Locators.getStartedButton);
    featuresLink=this.ElementLocator(Locators.featuresLink);
    docLink=this.ElementLocator(Locators.docLink)


    //Open browser
    async OpenBrowser(url: string){
        browser.manage().window().maximize();
        await browser.get(url);
    }

    async maximiseBrowser(){
        await browser.manage().window().maximize();
    }

    async clickFeatures(){
        await this.featuresLink.click();
    }

   async clickDoc(){
       await this.docLink.click();
   }

}

