import { browser, element, by, protractor, $$, $ } from "protractor";
import { IdentificationType, BasePage } from "./BasePage";
import { expect } from "chai";

const Locators = {
  whatIsAngularText: {
    type: IdentificationType[IdentificationType.Css],
    value: "#what-is-angular"
  }
};
export class DocPage extends BasePage {
  whatIsAngularText = this.ElementLocator(Locators.whatIsAngularText);

  async getAngularText() {
    browser.sleep(3000);
    await this.whatIsAngularText.getText().then(function(text) {
      expect(text).to.be.equal("What is Angular?");
    });
  }
}
