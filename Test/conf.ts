

import { browser } from "protractor";
var HtmlReporter = require('protractor-beautiful-reporter');

 
exports.config = {
  directConnect: false,
  framework: "jasmine2",

  seleniumAddress: "http://localhost:4444/wd/hub",
  capabilities: {
        browserName: 'chrome'
      },
 
  onPrepare: function() {
    // returning the promise makes protractor wait for the reporter config before executing tests
    return browser.getProcessedConfig().then(function(config) {
      // you could use other properties here if you want, such as platform and version
      var browserName = config.capabilities.browserName;
     
      
      jasmine.getEnv().addReporter(new HtmlReporter({
        baseDirectory: './testresults',
        jsonsSubfolder: 'json',
        screenshotsSubfolder: 'images',
        docTitle: 'Report',
        docName: 'Report.html',
        preserveDirectory: true,
        gatherBrowserLogs: true
      }).getJasmine2Reporter());

    });
    
  },
  specs: [ './FirstTestSpec.js' ],
}
 
  //specs: ['./LoginTestSpec.js'],
  //   specs: [ 
  //     './LoginTestSpec.js',
  //     './LandingTestSpec.js',
  //     './AreaTestSpec.js',
  //     './SourceTestSpec.js',
  //     './LocationTestSpec.js',
  //     './VatTestSpec.js',
  //     './ReasonsTestSpec.js',
  //     './SitesequenceTestSpec.js',
  //     './WebServiceLocationsTestSpec.js',
  //     './SystemSequenceTestSpec.js',
  //     './AccountsTestSpec.js',
  //     './ItemPropertyTestSpec.js',
  //     './ItemConfigTestSpec.js',
  //     './MachineTestSpec.js',
  //     './RangeTestSpec.js',
  //     './PrinterTestSpec.js',
  //     './CardLimitTestSpec.js',
  //     './CashupGroupTestSpec.js',
  //     './GenerallookupTestSpec.js',
  //     './AutoFundTestSpec.js',
  //     './MerchantTestSpec.js',
  //     './CBTTestSpec.js',
  //     './BagTestSpec.js',
  //     './MaskTestSpec.js',
  //     './UserMonitorTestSpec.js',
  //     './SeverityTestSpec.js',
  //     './SmartClientTestSpec.js',
  //     './PayTestspec.js',
  //     './CheckdigitTestSpec.js',
  //     './GroupMaintTestSpec.js'
  // ],

 

  






// import {Config, browser} from 'protractor';
// var HtmlReporter=require('protractor-beautiful-reporter');

// export let config: Config = {
//   framework: 'jasmine',
//   capabilities: {
//     browserName: 'chrome'
//   },

// onPrepare: function() {
// // returning the promise makes protractor wait for the reporter config before executing tests
// return browser.getProcessedConfig().then(function(config) {
// // you could use other properties here if you want, such as platform and version
// var browserName=config.capabilities.browserName;


 
// jasmine.getEnv().addReporter(new HtmlReporter({
// baseDirectory:'./testresults',
// jsonsSubfolder:'json',
// screenshotsSubfolder:'images',
// docTitle:'IM Rewrite Report',
// docName:'IM_Rewrite_Report.html',
// preserveDirectory:true,
// gatherBrowserLogs:true
// }).getJasmine2Reporter());
 
// });
// },
//   specs: [ './FirstTestSpec.js' ],
//   seleniumAddress: 'http://localhost:4444/wd/hub',

//   // You could set no globals to true to avoid jQuery '$' and protractor '$'
//   // collisions on the global namespace.
//   noGlobals: true
// };